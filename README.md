### QubesOS VM packages for Alpine Linux

# Deprecation notice: development has moved to a [new repo](https://lab.ilot.io/ayakael/qubes-aports)

#### Provided packages

Use `abuild-r` to build the following packages.
For more information on how to build an Alpine Package, read [this](https://wiki.alpinelinux.org/wiki/Creating_an_Alpine_package)

Core VM packages
  * qubes-vm-xen - Qubes's version of xen
  * qubes-libvchan-xen - libvchan library dependency
  * qubes-db-vm - qubes-db package
  * qubes-vm-utils - qubes-meminfo-writer service package
  * qubes-vm-core - Core init.d / qubes scripts
  * qubes-vm-gui-dev - Library dependencies for `qubes-vm-gui`
  * qubes-vm-gui - GUI agent
  * qubes-vm-qrexec - qrexec agent
  * qubes-gpg-split
  * qubes-usb-proxy
  * qubes-meta-packages - Meta package that pulls everything when added to world

Extra packages
  * qubes-pass - Aport for Rudd-O's inter-VM password manager for Qubes OS
  * qubes-dmenu - Custom build of dmenu for personal use

#### Drawbacks
QubesOS does not by default support openrc based templates. All of the init scripts have thus been implemented.
This might mean some bugs along the way, thus this project is still very much considered a WIP. Although I use
these packages on a daily basis, I can't test for all use-cases.

#### The yet-to-be-implemented list 
Thus the following use cases are still not supported:
 * Service VMs (sys-net, sys-usb, sys-firewall)
 * Firewall (not tested)
 * `qubes-builder` hooks (thus no `dom0` template RPM yet, see [here](https://gitlab.alpinelinux.org/ayakael/qubes-builder-alpine) for progress)
 * `apk` proxying from within template (thus you must allow internet access to template to install packages)
 * `qubes-vm-kernel-support` Not adapted for use on Alpine yet, due to it providing a Dracut module. In most cases, it is not necessary as Qubes provides the kernel. 
   This package is only neccessary when VM uses its own kernel, thus a hook is added to Dracut to generate the initrd for use within qubes.

#### Issues, recommendations and proposals
**To report an issue or share a recommendation**

Go [here](https://gitlab.alpinelinux.org/ayakael/qubes-alpine/-/issues)

**To make a merge request**
 * Fork the repo from Alpine's GitLab [here](https://gitlab.alpinelinux.org/ayakael/qubes-alpine)
 * Clone your fork locally. (`git clone $repo`)
 * Make a branch with a descriptive name (`git checkout -b $descriptivename`)
 * Make the changes you want to see in the world, commit, and push to the GitLab's remote repo
 * Request a merge [here](https://gitlab.alpinelinux.org/ayakael/qubes-alpine/-/merge_requests)

### Installation steps

It might require a few tweaks as these steps havn't been tested to their fullest extent. Some things
may have been forgotten along the way

#### 0. HVM Setup

* Create a new HVM by following [this guide](https://www.qubes-os.org/doc/standalones-and-hvms/#creating-an-hvm)
* Set the memory to fixed 4G
* Execute `qvm-prefs <vmname> kernel ""` - Otherwise it will not start
* For network, ensure you've followed [this part of the guide](https://www.qubes-os.org/doc/standalones-and-hvms/#setting-up-networking-for-hvms)
* Use `alpine-setup` to install the system on `/dev/xvda`, and feed it the network information from the previous step.
* Make sure your partition setup looks like this, and uses gpt partition scheme. (note the name for xvda3 as "Root filesystem")
```
/dev/xvda1 200M EFI System
/dev/xvda2 2048K BIOS boot partition
/dev/xvda3 (whatever) Root filesystem
```

#### 1. The metapackage

Following [this guide](https://wiki.alpinelinux.org/wiki/Creating_an_Alpine_package), you can setup a build environment where you can build this repo's packages. After, pointing to your local repo in `/etc/apk/repositories` (usually `/home/user/packages/main`), you can run `apk add qubes-vm-dependencies`

This will install everything required.

You may also use my personal Alpine repo [here](https://repo.gpg.nz/apk/) by adding it to your `/etc/apk/repositories`. Note that I make no guarantees of its reliability.

#### 2. Services and Checks

**Ensure that:**

 * `user ALL=(ALL) ALL` is present in `/etc/sudoers`
 * `/home/user` does exist and contains the usual skeleton
 * The user "user" and group "user" do exist.
 * `hvc0::respawn:/sbin/getty -L hvc0 115200 vt220` is in `/etc/inittab`
 * `eudev` is installed
 * The following is in `/etc/fstab`:
```
/dev/mapper/dmroot	/	ext4	defaults,discard,noatime	1 1
/dev/xvdb	/rw	auto	noauto,defaults,discard,nosuid,nodev	1 2
/dev/xvdc1      swap                    swap    defaults        0 0
/rw/home	/home	none	noauto,bind,defaults,nosuid,nodev	0 0
/rw/usrlocal	/usr/local	none	noauto,bind,defaults	0 0
none /dev/shm tmpfs defaults,size=1G 0 0
```

**Enable the services:**

Using `rc-update add $service`, add the following services:
 * udev
 * udev-trigger
 * xendriverdomain
 * qubes-qrexec-agent
 * qubes-db
 * qubes-meminfo-qriter
 * qubes-sysinit
 * qubes-core-early
 * qubes-core
 * qubes-gui-agent

#### 3. Test boot

You may now try a reboot and hope it doesn't end up in the rescue console.
If the boot is successful you should be able to use all the usual qvm commands,
pass devices, use the gui-agent, etc.

For debugging any `gui-agent` issues, set ensure that the VM type is `hvm` and access console via
`xl console $vm`

Final test by trying to run a Xorg based program, in my case: `qvm-run $vm urxvt`

#### 4. Converting to template
**Do the following**
 * convert to template using the following command, which also sets custom kernelopts:
 `qvm-clone --class TemplateVM --property virt_mode=pvh --property kernel='modules=ext4 rootfstype=ext4' $vm $template`
 * From within Qubes Manager, ensure that memory balancing is on.
 * test the template by running `qvm-run $template $gui-program`
 * APK proxying is a known-issue, please allow internet to template

You should now be able to install your own packages and setup your template to fit your use case.
