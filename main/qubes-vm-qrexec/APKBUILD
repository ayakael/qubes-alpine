# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>
# Contributor: Antoine Martin (ayakael) <dev@ayakael.net>

pkgname=qubes-vm-qrexec
subpackages="$pkgname-openrc $pkgname-doc"
pkgver=4.1.16
_gittag="v$pkgver"
pkgrel=2
pkgdesc="The Qubes qrexec files (qube side)"
arch="x86_64"
url="https://github.com/QubesOS/qubes-core-qrexec"
license='GPL'
depends="qubes-libvchan-xen"
options="!check" # No testsuite
makedepends="
	gcc
	make
	pandoc
	pkgconf 
	py3-setuptools
	qubes-libvchan-xen
	"
source="
	$pkgname-$_gittag.tar.gz::https://github.com/QubesOS/qubes-core-qrexec/archive/refs/tags/$_gittag.tar.gz
	qubes-qrexec-agent.openrc
	"
builddir="$srcdir/qubes-core-qrexec-${_gittag/v}"

prepare() {
	# remove all -Werror
	msg "Eradicating -Werror..."
	find . \( -name '*.mk' -o -name 'Make*' \) -exec sed -i -e 's/-Werror//g' {} +
}

build() {
	make all-base
	make all-vm

	# change all shebangs to bash as expected
	for i in $(grep '/bin/sh' -RlI .); do
		sed -i 's|/bin/sh|/bin/bash|' "$i"
	done
}

package() {
	make install-base DESTDIR="$pkgdir" SBINDIR=/sbin LIBDIR=/usr/lib SYSLIBDIR=/lib
	make install-vm DESTDIR="$pkgdir" SBINDIR=/sbin LIBDIR=/usr/lib SYSLIBDIR=/lib
	install -Dm 755 "$srcdir"/qubes-qrexec-agent.openrc "$pkgdir"/etc/init.d/qubes-qrexec-agent
}
sha512sums="
cdb76a6d66f9eda000c1dc1dcd450fd8d059198260b6e50f01416d6b707069aa396e4dcab5b248cd4f491aa5caf43acbaca5face65540c59825660e5175e9aef  qubes-vm-qrexec-v4.1.16.tar.gz
e2dd5cace82e881c40d5d37c69f7327fbabde81c9d23283de23de9f1197b7b018ef07a8d90e95c61bd249426d9d8297e7cb372333245941ffa0682c90ea3461f  qubes-qrexec-agent.openrc
"
