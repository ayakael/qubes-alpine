# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>

pkgname=qubes-dmenu
pkgver=0.38.0
_gittag="a3de0c2ece478aaeaaff5aec60887e683f2a8ce0"
_patchertag="6417335f09f0d2fb2faec78c5702a1ef9a19ab02"
_patchlist="
	line-height
	fuzzymatch
	fuzzyhighlight
	numbers
	xyw
	border
"
pkgrel=3
pkgdesc="A patch-friendly dmenu distribution"
arch="all"
url="https://github.com/jaimecgomezz/dmenu"
license="MIT"
depends="
	bash
	freetype
	python3
	"
makedepends="
	fontconfig
	libx11-dev
	libxft-dev
	libxinerama-dev
	"
subpackages="$pkgname-doc"
options="!check"
source="
	$pkgname-$_gittag.tar.gz::https://github.com/jaimecgomezz/dmenu/archive/$_gittag.tar.gz
	suckless-patchers-$_patchertag.tar.gz::https://github.com/jaimecgomezz/suckless-patchers/archive/$_patchertag.tar.gz
	qvm-passmenu.sh
	"
builddir="$srcdir"/dmenu-$_gittag

prepare() {
	default_prepare

	if [ ! -f handle ]; then
		cp "$srcdir"/suckless-patchers-$_patchertag/handle .
		# neutering handle from building
		sed -i '/build_distro/d' handle
	fi
	msg "Applying patches with handle"
	for i in $_patchlist; do
		echo "Applying $i"
		./handle patch $i
	done
}

build() {
	make all
}

package() {
	make DESTDIR="$pkgdir" PREFIX="/usr" install
	install -Dm755 "$builddir"/scripts/* "$pkgdir"/usr/bin/.
	install -Dm755 "$srcdir"/qvm-passmenu.sh "$pkgdir"/usr/bin/qvm-passmenu
}
sha512sums="
5d51ce72d83c5e4f4c010c9555f219742367b9828f75d103a074e105353602c511a5e80b0f297f7b5bf6c591883a2adae8b16153385f22932b1d493c6afacdf4  qubes-dmenu-a3de0c2ece478aaeaaff5aec60887e683f2a8ce0.tar.gz
4f8a0acb3655806981e3f302bc762292613b05141b568177e573b939bf6163ba950936a27febdaeb2939c660fbbbc943a24c6733cddeb0927fb24eef9fdf618d  suckless-patchers-6417335f09f0d2fb2faec78c5702a1ef9a19ab02.tar.gz
6b0c299253f902b5997696ba8780f2ad41c086b063035190565fc0c2c99d7e5f7236685a035db89972bf51029be7acd45f89eb634e15f7a2fd6998d6190c204d  qvm-passmenu.sh
"

