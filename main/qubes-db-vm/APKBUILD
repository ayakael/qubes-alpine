# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>
# Contributor: Antoine Martin (ayakael) <dev@ayakael.net>

pkgname=qubes-db-vm
subpackages="$pkgname-openrc"
pkgver=4.1.12
pkgrel=6
_gittag="v$pkgver"
pkgdesc="QubesDB libs and daemon service."
arch="x86_64"
url="https://github.com/QubesOS/qubes-core-qubesdb"
license='GPL'
options="!check" # No testsuite
depends="
	python3
	qubes-libvchan-xen
	"
makedepends="
	python3-dev
	qubes-libvchan-xen
	"
source="
	$pkgname-$_gittag.tar.gz::https://github.com/QubesOS/qubes-core-qubesdb/archive/refs/tags/$_gittag.tar.gz
	0001-musl-build.patch
	0001-create_pidfile.patch
	qubes-db.openrc
	"
builddir="$srcdir"/qubes-core-qubesdb-$pkgver

build() {
	# Build all with python bindings
	make all BACKEND_VMM=xen SYSTEMD=0

	# replace all shebangs with /bin/sh as qubes expects bash
	for i in $(grep '/bin/sh' -Rl .); do
		sed -i 's|/bin/sh|/bin/bash|' "$i"
	done
}

package() {
	# Install all with python bindings
	make install DESTDIR=$pkgdir LIBDIR=/usr/lib BINDIR=/usr/bin SBINDIR=/sbin
	install -Dm 755 "$srcdir"/qubes-db.openrc "$pkgdir"/etc/init.d/qubes-db
}
sha512sums="
80984c5c07bdf285d3a30f5d67c20ca9f12a3dcb7b3259ed08a3567101fbb1de9b6e9e83c6d2c5be9f1bbb080d02c5884131ae924a6663d7507fead41b9fa68d  qubes-db-vm-v4.1.12.tar.gz
af86268c264c843b94f9cefb735b9d078dc58819c890fc0a31dd79fa2761d3c2fa87aed73752bca1db07948ba86ecfe16a745b19672ccc10dfb9461df24aa207  0001-musl-build.patch
ffe9ea8f65b4e164c3a0d1c8762d1e3b39de3799ae3e63f825457d52de49c6522820950e6262deaa9235ad97cd7c60bf1c9a077fff716c4ca9dbd688e9a73c91  0001-create_pidfile.patch
3d87f82d3637cf10bf1a3058ebbd2590ab17f65d1b49058f62d892f126635497abd5045f6797bc8069e5de08bb6e08fc6146deb6422090ad02122764cc6d72f0  qubes-db.openrc
"
